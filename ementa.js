var entradas = [];
var bebidas = [];
var pratosprincipais = [];
var sobremesas = [];
var pedido = [];
var ementa;
var recomendacoes;
var custoTotal = 0;
var nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
  20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
  30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
  40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
  50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
  60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
  70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
  80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
  90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
  100];


/* CustomMenu */
var frutas = [];
var alcool = [];
var carnes = [];
var acompanhamentos = [];
var options = [];

function createOption(type, name, price, img) {
    this.type = type;
    this.way = "";
    this.name = name;
    this.price = price;
    this.img = img;
}

frutas.push(new createOption("fr", "Ananás", 0.4, "images/mealsoption/fruta/ananas.jpg"));
frutas.push(new createOption("fr", "Banana", 0.4, "images/mealsoption/fruta/banana.jpg"));
frutas.push(new createOption("fr", "Kiwi", 0.4, "images/mealsoption/fruta/kiwi.jpg"));
frutas.push(new createOption("fr", "Laranja", 0.4, "images/mealsoption/fruta/laranja.jpg"));
frutas.push(new createOption("fr", "Maçã", 0.4, "images/mealsoption/fruta/maca.jpg"));
frutas.push(new createOption("fr", "Manga", 0.4, "images/mealsoption/fruta/manga.jpg"));
frutas.push(new createOption("fr", "Maracujá", 0.4, "images/mealsoption/fruta/maracuja.jpg"));
frutas.push(new createOption("fr", "Melancia", 0.4, "images/mealsoption/fruta/melancia.jpg"));
frutas.push(new createOption("fr", "Morango", 0.4, "images/mealsoption/fruta/morango.jpg"));
frutas.push(new createOption("fr", "Pera", 0.4, "images/mealsoption/fruta/pera.jpg"));
frutas.push(new createOption("fr", "Pêssego", 0.4, "images/mealsoption/fruta/pessego.jpg"));

alcool.push(new createOption("al", "Gin", 2, "images/mealsoption/alcool/gin.jpg"));
alcool.push(new createOption("al", "Vodka", 2, "images/mealsoption/alcool/vodka.jpg"));
alcool.push(new createOption("al", "Rum", 2, "images/mealsoption/alcool/rum.jpg"));
alcool.push(new createOption("al", "Whiskey", 2, "images/mealsoption/alcool/whisky.jpg"));
alcool.push(new createOption("al", "Tequila", 2, "images/mealsoption/alcool/tequila.jpg"));

carnes.push(new createOption("cr", "Carne de Vaca-Grelhada", 3, "images/mealsoption/carne/vacag.jpg"));
carnes.push(new createOption("cr", "Carne de Vaca-Frita", 3, "images/mealsoption/carne/vacaf.jpg"));
carnes.push(new createOption("cr", "Carne de Vaca-Assada", 3, "images/mealsoption/carne/vacaa.jpg"));
carnes.push(new createOption("cr", "Carne de Porco-Grelhada", 3, "images/mealsoption/carne/porcog.jpg"));
carnes.push(new createOption("cr", "Carne de Porco-Frita", 3, "images/mealsoption/carne/porcof.jpg"));
carnes.push(new createOption("cr", "Carne de Porco-Assada", 3, "images/mealsoption/carne/porcoa.jpg"));
carnes.push(new createOption("cr", "Frango", 3, "images/mealsoption/carne/frango.jpg"));
carnes.push(new createOption("cr", "Vitela", 3, "images/mealsoption/carne/vitela.jpg"));
carnes.push(new createOption("cr", "Pato", 3, "images/mealsoption/carne/pato.jpg"));
carnes.push(new createOption("cr", "Tofu", 3, "images/mealsoption/carne/tofu.jpg"));
carnes.push(new createOption("cr", "Tempeh", 3, "images/mealsoption/carne/tempeh.jpg"));
carnes.push(new createOption("cr", "Seitan", 3, "images/mealsoption/carne/seitan.jpg"));

acompanhamentos.push(new createOption("cd", "Arroz Simples", 0.5, "images/mealsoption/acompanhamento/arroz.jpg"));
acompanhamentos.push(new createOption("cd", "Batata Frita", 0.5, "images/mealsoption/acompanhamento/batatafrita.jpg"));
acompanhamentos.push(new createOption("cd", "Batata Cozida", 0.5, "images/mealsoption/acompanhamento/batatacozida.jpg"));
acompanhamentos.push(new createOption("cd", "Arroz de Tomate", 0.5, "images/mealsoption/acompanhamento/arroztomate.jpg"));
acompanhamentos.push(new createOption("cd", "Batata Doce ", 0.5, "images/mealsoption/acompanhamento/batatadoce.jpg"));
acompanhamentos.push(new createOption("cd", "Batata Assada", 0.5, "images/mealsoption/acompanhamento/batataassada.jpg"));
acompanhamentos.push(new createOption("cd", "Puré de Batata", 0.5, "images/mealsoption/acompanhamento/purebatata.jpg"));
acompanhamentos.push(new createOption("cd", "Alface", 0.5, "images/mealsoption/acompanhamento/alface.jpg"));
acompanhamentos.push(new createOption("cd", "Tomate", 0.5, "images/mealsoption/acompanhamento/tomate.jpg"));
acompanhamentos.push(new createOption("cd", "Rúcula", 0.5, "images/mealsoption/acompanhamento/rucula.jpg"));
acompanhamentos.push(new createOption("cd", "Pepino", 0.5, "images/mealsoption/acompanhamento/pepino.jpg"));
acompanhamentos.push(new createOption("cd", "Ovo Estrelado", 0.5, "images/mealsoption/acompanhamento/ovoestrelado.jpg"));
acompanhamentos.push(new createOption("cd", "Ovo Cozido", 0.5, "images/mealsoption/acompanhamento/ovocozido.jpg"));

function goToCustom(id) {
    showOptions();
    hide("BigDivBoys");
    show("BigCustomDiv");
    document.getElementById("Ingredientes").appendChild(showIngredientes(id));
}

function addToCustomPedido(option) {
    if (options.length < 4) {
        for (var i = 0; i < options.length; i++) {
            if (options[i].name == option.name) {
                repeatCustom();
                showOptions();
                return;
            }
        }
        options.push(option);
    } else {
        overCustom();
    }
    showOptions();
}

function repeatCustom() {
    var modal = document.getElementById('repeatCustomModal');
    var span = document.getElementsByClassName("close")[3];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function showIngredientes(id) {
    var i1;
    var i2;
    if (id == "Bebida") {
        i1 = frutas;
        i2 = alcool;
    }
    if (id == "Prato Principal") {
        i1 = carnes;
        i2 = acompanhamentos;
    }
    var division = document.createElement("div");
    for (var i = 0; i < i1.length; i++) {
        var item = document.createElement('table');
        var bigrow = document.createElement('tr');
        for (var j = 0; j < 4 && i < i1.length; j++, i++) {
            var tdiv = document.createElement('td');
            tdiv.className = "ementa-entry";
            var tableimgrow = document.createElement('tr');
            var tdimg = document.createElement('td');
            var tableinfrow = document.createElement('tr');
            var tdinfo = document.createElement('td');
            var img = document.createElement('img');
            img.className = "option-pic";
            img.src = i1[i].img;
            img.onclick = (function(i) {
                return function() {
                    addToCustomPedido(i1[nums[i]]);
                };
            })(nums[i]);
            tdimg.appendChild(img);
            tdinfo.className = "option-name";
            tdinfo.appendChild(document.createTextNode(i1[i].name + " - " + i1[i].price.toFixed(2) + "€"));
            tableimgrow.appendChild(tdimg);
            tableinfrow.appendChild(tdinfo);
            tdiv.appendChild(tableimgrow);
            tdiv.appendChild(tableinfrow);
            bigrow.appendChild(tdiv);
            if (j == 3) {
                i--;
            }
        }
        item.appendChild(bigrow);
        division.appendChild(item);
    }
    var aux = document.createElement('div');
    aux.style.height = "20pt";
    division.appendChild(aux);
    for (var i = 0; i < i2.length; i++) {
        var item = document.createElement('table');
        var bigrow = document.createElement('tr');
        for (var j = 0; j < 4 && i < i2.length; j++, i++) {
            var tdiv = document.createElement('td');
            tdiv.className = "ementa-entry";
            var tableimgrow = document.createElement('tr');
            var tdimg = document.createElement('td');
            var tableinfrow = document.createElement('tr');
            var tdinfo = document.createElement('td');
            var img = document.createElement('img');
            img.className = "option-pic";
            img.src = i2[i].img;
            img.onclick = (function(i) {
                return function() {
                    addToCustomPedido(i2[nums[i]]);
                };
            })(nums[i]);
            tdimg.appendChild(img);
            tdinfo.className = "option-name";
            tdinfo.appendChild(document.createTextNode(i2[i].name + " - " + i2[i].price.toFixed(2) + "€"));
            tableimgrow.appendChild(tdimg);
            tableinfrow.appendChild(tdinfo);
            tdiv.appendChild(tableimgrow);
            tdiv.appendChild(tableinfrow);
            bigrow.appendChild(tdiv);
            if (j == 3) {
                i--;
            }
        }
        item.appendChild(bigrow);
        division.appendChild(item);
    }
    return division;
}

function forgetCustom() {
    options = [];
    removeCustom();
    hide("BigCustomDiv");
    show("BigDivBoys");
}

function confirmCustom() {
    if (options.length == 0) {
        noIngredModal();
        return;
    }
    if (options[0].type == "fr" || options[0].type == "al") {
        var customPedido = new createMeal("custom", "Bebida Customizada", 0, 0, "");
    } else {
        var customPedido = new createMeal("custom", "Prato Customizado", 0, 0, "");

    }
    var preco = 0;
    for (var i = 0; i < options.length; i++) {
        customPedido.options.push(options[i]);
        preco += options[i].price;
    }
    customPedido.img = customPedido.options[0].img;
    customPedido.price = preco;
    customPedido.units = 1;
    options = [];
    pedido.push(customPedido);
    showPedido();
    removeCustom();
    hide("BigCustomDiv");
    show("BigDivBoys");

}

function removeCustom() {
    var ingredientes = document.getElementById("Ingredientes");
    while (ingredientes.hasChildNodes()) {
        ingredientes.removeChild(ingredientes.firstChild);
    }
}

function removeFromCustom(option) {
    var index = options.indexOf(option);
    options.splice(index, 1);
    showOptions();
}

function showOptions() {
    var total = 0;
    var ped = document.getElementById('IngPed');
    while (ped.hasChildNodes()) {
        ped.removeChild(ped.firstChild);
    }
    for (var i = 0; i < options.length; i++) {
        total += options[i].price;
        var table = document.createElement('table');
        var item = document.createElement('li');
        item.style.maxWidth = "360px";
        item.style.marginLeft = "10pt";
        item.style.fontSize = "12pt";
        item.appendChild(document.createTextNode(options[i].name + " = " + options[i].price.toFixed(2) + "€"));
        var minus = document.createElement("img");
        minus.style.marginLeft = "10px";
        minus.src = "images/minus.png";
        minus.style.float = "right";
        minus.onclick = (function(i) {
            return function() {
                removeFromCustom(options[nums[i]]);
            };
        })(nums[i]);
        minus.className = "minusSign";
        item.appendChild(minus);
        table.appendChild(item);
        ped.appendChild(table);
    }
    if (total != 0) {
        var totaltext = document.createElement('p');
        totaltext.appendChild(document.createTextNode("Preço total: " + total.toFixed(2) + "€"));
        totaltext.style.fontSize = "14pt";
        ped.appendChild(totaltext);
    }
}
/*---------------*/

function createMeal(type, name, price, rating, img) {
    this.units = 0;
    this.numratings = 0;
    this.type = type;
    this.name = name;
    this.price = price;
    this.rating = rating;
    this.img = img;
    this.options = [];
    return this;
}

entradas.push(new createMeal("en", "Pão de alho", 2.00, 0, "images/meals/entrada/paodealho.jpg"));
entradas.push(new createMeal("en", "Cocktail de Camarão", 4.00, 0, "images/meals/entrada/cocktailcamarao.jpg"));
entradas.push(new createMeal("en", "Ameijoas à Bulhão Pato", 2.50, 0, "images/meals/entrada/ameijoasbulhaopato.jpg"));
entradas.push(new createMeal("en", "Sopa de Legumes", 2.00, 4, "images/meals/entrada/sopadelegumes.jpg"));
entradas.push(new createMeal("en", "Sopa de Peixe", 2.40, 0, "images/meals/entrada/sopadepeixe.jpg"));
entradas.push(new createMeal("en", "Pão Alentejano", 0.90, 0, "images/meals/entrada/paoalentejano.jpg"));
entradas.push(new createMeal("en", "Manteiga", 0.60, 5, "images/meals/entrada/manteiga.jpg"));
entradas.push(new createMeal("en", "Salada de Atum", 2.70, 0, "images/meals/entrada/saladaatum.jpg"));
entradas.push(new createMeal("en", "Salada de Tomate", 1.90, 4, "images/meals/entrada/saladatomate.jpg"));

bebidas.push(new createMeal("be", "Água Mineral sem Gás", 0.90, 0, "images/meals/bebida/aguafastio.jpg"));
bebidas.push(new createMeal("be", "Fanta de Laranja", 1.50, 0, "images/meals/bebida/fantalaranja.jpg"));
bebidas.push(new createMeal("beal", "Vinho do Porto", 2.50, 4, "images/meals/bebida/vinhodoporto.jpg"));
bebidas.push(new createMeal("be", "Coca-Cola", 1.50, 0, "images/meals/bebida/cocacola.jpg"));
bebidas.push(new createMeal("be", "Ice Tea de Pêssego", 1.50, 0, "images/meals/bebida/iceteapessego.jpg"));
bebidas.push(new createMeal("be", "Ice Tea de Limão", 1.50, 0, "images/meals/bebida/icetealimao.jpg"));
bebidas.push(new createMeal("be", "Ice Tea de Manga", 1.50, 0, "images/meals/bebida/iceteamanga.jpg"));
bebidas.push(new createMeal("beal", "Sagres", 1.20, 5, "images/meals/bebida/sagres.jpg"));
bebidas.push(new createMeal("beal", "Caneca Cerveja", 3.00, 0, "images/meals/bebida/canecacerveja.jpg"));
bebidas.push(new createMeal("beal", "Martini", 1.00, 3, "images/meals/bebida/martini.jpg"));

pratosprincipais.push(new createMeal("pp", "Bife da Vazia", 12.50, 0, "images/meals/pratoprincipal/bifedavazia.jpg"));
pratosprincipais.push(new createMeal("pp", "Bife do Lombo", 15.50, 4, "images/meals/pratoprincipal/bifedolombo.jpg"));
pratosprincipais.push(new createMeal("pp", "Cabrito Assado no Forno", 10.50, 0, "images/meals/pratoprincipal/cabritonoforno.jpg"));
pratosprincipais.push(new createMeal("pp", "Almôndegas com Molho de Tomate", 9.30, 0, "images/meals/pratoprincipal/almondegas.jpg"));
pratosprincipais.push(new createMeal("pp", "Arroz de Frango com Farinheira", 11.50, 5, "images/meals/pratoprincipal/arrozfrango.jpeg"));
pratosprincipais.push(new createMeal("pp", "Arroz de Pato", 9.50, 0, "images/meals/pratoprincipal/arrozpato.jpg"));
pratosprincipais.push(new createMeal("pp", "Arroz de Bacalhau", 11.60, 0, "images/meals/pratoprincipal/arrozbacalhau.jpg"));
pratosprincipais.push(new createMeal("pp", "Bacalhau com Natas", 12.10, 0, "images/meals/pratoprincipal/bacalhaucomnatas.jpg"));
pratosprincipais.push(new createMeal("pp", "Espetadas de Peixe", 10.50, 4, "images/meals/pratoprincipal/espetadaspeixe.jpeg"));
pratosprincipais.push(new createMeal("pp", "Filetes de Peixe Escalfados com Legumes e Molho Manga", 14.20, 0, "images/meals/pratoprincipal/filetespeixe.jpg"));
pratosprincipais.push(new createMeal("pp", "Lombos de Salmão com Funcho", 16.60, 0, "images/meals/pratoprincipal/lombosalmao.jpg"));
pratosprincipais.push(new createMeal("pp", "Arroz de Ervilhas e Cogumelos", 13.50, 5, "images/meals/pratoprincipal/arrozervilhacogumelos.jpeg"));
pratosprincipais.push(new createMeal("pp", "Hambúrguer Vegan", 15.20, 0, "images/meals/pratoprincipal/hamburguervegan.jpg"));
pratosprincipais.push(new createMeal("pp", "Legumes à Brás", 14.40, 0, "images/meals/pratoprincipal/legumesbras.jpg"));
pratosprincipais.push(new createMeal("pp", "Risotto de Quinoa com Cogumelos", 12.00, 4, "images/meals/pratoprincipal/risottocogumelos.jpg"));

sobremesas.push(new createMeal("sm", "Cheesecake de Figo e Crumble de Noz e Canela", 4.00, 4, "images/meals/sobremesas/cheesecakefigo.jpg"));
sobremesas.push(new createMeal("sm", "Tarte Fria de Leite Condensado e Morangos", 4.20, 0, "images/meals/sobremesas/tartefriamorangos.jpg"));
sobremesas.push(new createMeal("sm", "Tarte de Leite Creme", 3.40, 0, "images/meals/sobremesas/tarteleitecreme.jpg"));
sobremesas.push(new createMeal("sm", "Molotof com Doce de Ovos", 3.60, 5, "images/meals/sobremesas/molotofdoceovos.jpg"));
sobremesas.push(new createMeal("sm", "Pudim de Chocolate", 3.50, 0, "images/meals/sobremesas/pudimchocolate.jpg"));
sobremesas.push(new createMeal("sm", "Semi-Frio de Morango com Palitos la Reine", 4.00, 4, "images/meals/sobremesas/semifriomorango.jpg"));
sobremesas.push(new createMeal("sm", "Bolo de Bolacha com Baba de Camelo", 4.50, 0, "images/meals/sobremesas/bolobolacha.jpg"));
sobremesas.push(new createMeal("sm", "Mousse de Chocolate", 3.00, 0, "images/meals/sobremesas/moussechocolate.jpg"));
sobremesas.push(new createMeal("sm", "Mousse de Laranja", 3.00, 0, "images/meals/sobremesas/mousselaranja.jpg"));
sobremesas.push(new createMeal("sm", "Mousse de Leite Condensado", 3.00, 3, "images/meals/sobremesas/mousseleitecondensado.jpg"));
sobremesas.push(new createMeal("sm", "Natas do Céu", 2.40, 0, "images/meals/sobremesas/natasdoceu.jpg"));


function showEmenta(id) {
    var division = document.createElement('div');
    if (id == "Entrada") {
        ementa = entradas;
    }
    if (id == "Bebida") {
        ementa = bebidas;
        var item = document.createElement('table');
        item.className = "ementa-entry";
        var tablerow = document.createElement('tr');
        var tdimg = document.createElement('td');
        var tdinfo = document.createElement('td');
        tdinfo.className = "meal-name";
        tdinfo.appendChild(document.createTextNode("Crie a sua Bebida"));
        var img = document.createElement('img');
        img.src = "images/mealsoption/drinkmixer.jpg";
        img.className = "meal-pic";
        img.onclick = (function(i) {
            return function() {
                goToCustom("Bebida");
            };
        })(nums[i]);
        tdimg.appendChild(img);
        tablerow.appendChild(tdimg);
        tablerow.appendChild(tdinfo);
        item.appendChild(tablerow);
        division.appendChild(item);
    }
    if (id == "Prato Principal") {
        ementa = pratosprincipais;
        var item = document.createElement('table');
        item.className = "ementa-entry";
        var tablerow = document.createElement('tr');
        var tdimg = document.createElement('td');
        var tdinfo = document.createElement('td');
        tdinfo.className = "meal-name";
        tdinfo.appendChild(document.createTextNode("Crie o seu Prato Principal"));
        var img = document.createElement('img');
        img.src = "images/mealsoption/cookicon.jpg";
        img.className = "meal-pic";
        img.onclick = (function(i) {
            return function() {
                goToCustom("Prato Principal");
            };
        })(nums[i]);
        tdimg.appendChild(img);
        tablerow.appendChild(tdimg);
        tablerow.appendChild(tdinfo);
        item.appendChild(tablerow);
        division.appendChild(item);
    }
    if (id == "Sobremesa") {
        ementa = sobremesas;
    }
    if (id == "Confirmar") {
        ementa = pedido;
    }


    for (var i = 0; i < ementa.length; i++) {
        var item = document.createElement('table');
        item.className = "ementa-entry";
        var tablerow = document.createElement('tr');
        var tdimg = document.createElement('td');
        var tdinfo = document.createElement('td');
        var tdplus = document.createElement('td');
        var img = document.createElement('img');
        var plus = document.createElement('img');
        img.className = "meal-pic";
        img.src = ementa[i].img;
        img.onclick = (function(i) {
            return function() {
                addToPedido(pedido, ementa[nums[i]]);
            };
        })(nums[i]);
        tdimg.appendChild(img);
        tdinfo.className = "meal-name";
        tdinfo.appendChild(document.createTextNode(ementa[i].name + " - " + ementa[i].price.toFixed(2) + "€"));
        plus.className = "plusSign";
        plus.src = "images/plus.png";
        plus.onclick = (function(i) {
            return function() {
                addToPedido(pedido, ementa[nums[i]]);
            };
        })(nums[i]);
        tdplus.appendChild(plus);
        tablerow.appendChild(tdimg);
        tablerow.appendChild(tdinfo);
        tablerow.appendChild(tdplus);
        item.appendChild(tablerow);
        division.appendChild(item);
    }
    return division;
}

function showRecomendacoes(id) {
    if (id == "Entrada") {
        recomendacoes = entradas;
    }
    if (id == "Bebida") {
        recomendacoes = bebidas;
    }
    if (id == "Prato Principal") {
        recomendacoes = pratosprincipais;
    }
    if (id == "Sobremesa") {
        recomendacoes = sobremesas;
    }
    if (id == "Confirmar") {
        return;
    }
    var division = document.createElement('div');
    var item = document.createElement('table');
    var tablerow = document.createElement('tr');
    for (var i = 0; i < recomendacoes.length; i++) {
        if (recomendacoes[i].rating > 3) {
            var tablediv = document.createElement('td');
            tablediv.className = "recomendacao-entry";
            var tdimg = document.createElement('td');
            var auxrowinfo = document.createElement('tr');
            var auxrowstar = document.createElement('tr');
            var tdinfo = document.createElement('td');
            var tdplus = document.createElement('td');
            var img = document.createElement('img');
            var plus = document.createElement('img');
            var star = document.createElement('img');
            star.src = "images/star_on.png";
            star.className = "starDiv-big";
            auxrowstar.appendChild(star);
            auxrowstar.appendChild(document.createTextNode(recomendacoes[i].rating.toFixed(1)));
            img.className = "meal-pic";
            img.src = recomendacoes[i].img;
            img.onclick = (function(i) {
                return function() {
                    addToPedido(pedido, recomendacoes[nums[i]]);
                };
            })(nums[i]);
            tdimg.appendChild(img);
            auxrowinfo.className = "meal-name";
            auxrowinfo.appendChild(document.createTextNode(recomendacoes[i].name + " - " + recomendacoes[i].price.toFixed(2) + "€"));
            tdinfo.appendChild(auxrowstar);
            tdinfo.appendChild(auxrowinfo);
            plus.className = "plusSign";
            plus.src = "images/plus.png";
            plus.onclick = (function(i) {
                return function() {
                    addToPedido(pedido, recomendacoes[nums[i]]);
                };
            })(nums[i]);
            tdplus.appendChild(plus);
            tablediv.appendChild(tdimg);
            tablediv.appendChild(tdinfo);
            tablediv.appendChild(tdplus);
            tablerow.appendChild(tablediv);
        }
    }
    item.appendChild(tablerow);
    division.appendChild(item);
    return division;
}

function showConfirm() {
    var total = 0;
    var ped = document.getElementById('ConfirmScreen');
    while (ped.hasChildNodes()) {
        ped.removeChild(ped.firstChild);
    }
    for (var i = 0; i < pedido.length; i++) {
        total += pedido[i].price * pedido[i].units;
        var item = document.createElement('li');
        var itemimg = document.createElement('img');
        itemimg.src = pedido[i].img;
        itemimg.className = "meal-pic";
        item.className = "ementa-entry";
        item.appendChild(itemimg);
        //var table = createStars(i);
        item.style.marginLeft = "10pt";
        item.style.fontSize = "16pt";
        var unitcontainer = document.createElement('span');
        unitcontainer.appendChild(document.createTextNode(pedido[i].units + "x "));
        unitcontainer.style.fontWeight = "bold";

        item.appendChild(unitcontainer);
        item.appendChild(document.createTextNode(pedido[i].name + " = " + (pedido[i].price * pedido[i].units).toFixed(2) + "€"));
        if (pedido[i].type == "custom") {
            var uli = document.createElement('ul');
            for (var j = 0; j < pedido[i].options.length; j++) {
                var ili = document.createElement('li');
                ili.className = "ementa-entry";
                ili.appendChild(document.createTextNode(pedido[i].options[j].name));
                uli.appendChild(ili);
            }

            var table = document.createElement('table');
            var row = document.createElement('tr');
            var td1 = document.createElement('td');
            td1.appendChild(item);
            var td2 = document.createElement('td');
            td2.appendChild(uli);
            row.appendChild(td1);
            row.appendChild(td2);
            table.appendChild(row);
            ped.appendChild(table);
        } else {
            ped.appendChild(item);
        }
    }
    if (total != 0) {
        var totaltext = document.createElement('p');
        totaltext.appendChild(document.createTextNode("Preço total: " + total.toFixed(2) + "€"));
        totaltext.style.fontSize = "14pt";
        ped.appendChild(totaltext);
    }
}

function showRequested() {
    var ped = document.getElementById('PedidoRealizado');
    var cost = 0;
    while (ped.hasChildNodes()) {
        ped.removeChild(ped.firstChild);
    }
    for (var i = 0; i < pedido.length; i++) {
        cost += pedido[i].price * pedido[i].units;
        var item = document.createElement('li');
        item.style.marginLeft = "10pt";
        item.style.fontSize = "16pt";
        item.appendChild(document.createTextNode(pedido[i].name));
        if (pedido[i].type == "custom") {
            var uli = document.createElement('ul');
            for (var j = 0; j < pedido[i].options.length; j++) {
                var ili = document.createElement('li');
                ili.appendChild(document.createTextNode(pedido[i].options[j].name));
                uli.appendChild(ili);
            }
            item.appendChild(uli);
        } else {
            var table = createStars(i);
            item.appendChild(table);
        }
        ped.appendChild(item);
    }
    var totaltext = document.createElement('p');
    if (cost != 0) {
        if (mesaIndex == 0) {
            for (var i = 0; i < 5; i++) {
                var totalpedido = 0;
                for (var j = 0; j < mesas[i].length; j++) {
                    totalpedido += mesas[i][j].price * mesas[i][j].units;
                }
                var ptext = document.createElement('p');
                ptext.appendChild(document.createTextNode("Preço total do Cliente " + (i+1) + " : " + totalpedido.toFixed(2) + "€"))
                if (totalpedido != 0) {
                    totaltext.appendChild(ptext);
                }
            }
            cost = custoTotal;
        }
        totaltext.appendChild(document.createTextNode("Preço total: " + cost.toFixed(2) + "€"));
        totaltext.style.fontSize = "14pt";
        ped.appendChild(totaltext);
        ped.appendChild(totaltext);
    }
}

function addToPedido(ped, e) {
    var j = -1;
    for (var i = 0; i < ped.length; i++) {
        if (ped[i].name == e.name) {
            j = i;
            break;
        }
    }
    if (j != -1) {
        e = ped[j];
    } else {
        e = new createMeal(e.type, e.name, e.price, e.rating, e.img);
    }
    var modal = document.getElementById('overModal');
    var span = document.getElementsByClassName("close")[1];
    span.onclick = function() {
        modal.style.display = "none";
    }
    var yesBtn = document.getElementById("yesBtnOver");
    var noBtn = document.getElementById("noBtnOver");
    yesBtn.onclick = function() {
        e.units++;
        modal.style.display = "none";
        showPedido();
    }
    noBtn.onclick = function() {
        modal.style.display = "none";
    }
    if (e.units == 0) {
        e.units++;
        ped.push(e);
    } else if (e.units >= 5) {
        document.getElementById("overText").innerHTML = "Encomendar outra unidade de " + e.name + " ?";
        modal.style.display = "block";
    } else {
        e.units++;
    }
    showPedido();

}

function showPedido() {
    if (mesaIndex == 0) {
        showAllPedido();
    } else {
        var total = 0;
        var ped = document.getElementById('Pedido');
        while (ped.hasChildNodes()) {
            ped.removeChild(ped.firstChild);
        }
        for (var i = 0; i < pedido.length; i++) {
            total += pedido[i].price * pedido[i].units;
            var table = document.createElement('table');
            var item = document.createElement('li');
            item.style.maxWidth = "360px";
            item.style.marginLeft = "10pt";
            item.style.fontSize = "12pt";
            var unitcontainer = document.createElement('span');
            unitcontainer.appendChild(document.createTextNode(pedido[i].units + "x "));
            unitcontainer.style.fontWeight = "bold";
            item.appendChild(unitcontainer);
            item.appendChild(document.createTextNode(pedido[i].name + " = " + (pedido[i].price * pedido[i].units).toFixed(2) + "€"));
            var minus = document.createElement("img");
            minus.style.marginLeft = "10px";
            minus.src = "images/minus.png";
            minus.style.float = "right";
            minus.onclick = (function(i) {
                return function() {
                    removeFromPedido(pedido, pedido[nums[i]]);
                };
            })(nums[i]);
            minus.className = "minusSign";
            var plus = document.createElement("img");
            plus.style.marginLeft = "10px";
            plus.src = "images/smallplus.png"
            plus.style.float = "right";
            plus.onclick = (function(i) {
                return function() {
                    addToPedido(pedido, pedido[nums[i]]);
                };
            })(nums[i]);
            plus.className = "minusSign";
            item.appendChild(minus);
            item.appendChild(plus);
            if (pedido[i].type == "custom") {
                for (var j = 0; j < pedido[i].options.length; j++) {
                    var ili = document.createElement('li');
                    ili.appendChild(document.createTextNode("---- "+pedido[i].options[j].name));
                    item.appendChild(ili);
                }
            }
            table.appendChild(item);
            ped.appendChild(table);
        }
        if (total != 0) {
            var totaltext = document.createElement('p');
            totaltext.appendChild(document.createTextNode("Preço total: " + total.toFixed(2) + "€"));
            totaltext.style.fontSize = "14pt";
            ped.appendChild(totaltext);
        }
        custoTotal = total;
    }
}

function removeFromPedido(ped, e) {
    if (e.units == 1) {
        e.units--;
        var index = ped.indexOf(e);
        ped.splice(index, 1);
    } else {
        e.units--;
    }
    showPedido();
}

function createStars(i) {
    var table = document.createElement('table');
    var tablerow = document.createElement('tr');
    for (var j = 1; j <= 5; j++) {
        var tablediv = document.createElement('td');
        var star = document.createElement('img');
        star.className = "starDiv";
        star.id = "p" + i + "s" + j;
        star.src = "images/star_off.png";
        star.onclick = (function(i, j) {
            return function() {
                changeStar(nums[i], nums[j]);
            };
        })(nums[i], nums[j]);
        tablediv.appendChild(star);
        tablerow.appendChild(tablediv);
    }
    var tablediv = document.createElement('td');
    var confButton = document.createElement('div');
    var confText = document.createElement('p');
    var rateText = document.createTextNode("Classificar");
    confText.className = "confText";
    confText.appendChild(rateText);
    confButton.appendChild(confText);
    confButton.id = "p" + i + "c";
    confButton.className = "confButton";
    confButton.onclick = (function(i) {
        return function() {
            addRating(i, 0);
        };
    })(nums[i]);
    tablediv.appendChild(confButton);
    tablerow.appendChild(tablediv);;
    table.appendChild(tablerow);
    return table;
}

/*
star.onclick=(function(i,j) {
	return changeStar(i,j);
})(nums[i],nums[j]);
*/

function changeStar(i, j) {
    var ida = "p" + i + "s1";
    var idb = "p" + i + "s2";
    var idc = "p" + i + "s3";
    var idd = "p" + i + "s4";
    var ide = "p" + i + "s5";
    var id = "p" + i + "s" + j;
    console.log(id);
    if (id == ida) {
        document.getElementById(id).src = "images/star_on.png";
        document.getElementById(idb).src = "images/star_off.png";
        document.getElementById(idc).src = "images/star_off.png";
        document.getElementById(idd).src = "images/star_off.png";
        document.getElementById(ide).src = "images/star_off.png";
    }
    if (id == idb) {
        document.getElementById(ida).src = "images/star_on.png";
        document.getElementById(id).src = "images/star_on.png";
        document.getElementById(idc).src = "images/star_off.png";
        document.getElementById(idd).src = "images/star_off.png";
        document.getElementById(ide).src = "images/star_off.png";
    }
    if (id == idc) {
        document.getElementById(ida).src = "images/star_on.png";
        document.getElementById(idb).src = "images/star_on.png";
        document.getElementById(id).src = "images/star_on.png";
        document.getElementById(idd).src = "images/star_off.png";
        document.getElementById(ide).src = "images/star_off.png";
    }
    if (id == idd) {
        document.getElementById(ida).src = "images/star_on.png";
        document.getElementById(idb).src = "images/star_on.png";
        document.getElementById(idc).src = "images/star_on.png";
        document.getElementById(id).src = "images/star_on.png";
        document.getElementById(ide).src = "images/star_off.png";
    }
    if (id == ide) {
        document.getElementById(ida).src = "images/star_on.png";
        document.getElementById(idb).src = "images/star_on.png";
        document.getElementById(idc).src = "images/star_on.png";
        document.getElementById(idd).src = "images/star_on.png";
        document.getElementById(ide).src = "images/star_on.png";
    }
    var confid = "p" + i + "c";
    document.getElementById(confid).onclick = (function(i, j) {
        return function() {
            addRating(nums[i], nums[j]);
        }
    })(nums[i], nums[j]);
}

function addRating(ped, val) {
    var modal = document.getElementById('rateModal');
    var span = document.getElementsByClassName("close")[4];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
    var auxList = [];
    if (pedido[ped].type == "en") {
        auxList = entradas;
    } else if (pedido[ped].type == "be" || pedido[ped].type == "beal") {
        auxList = bebidas;
    } else if (pedido[ped].type == "pp") {
        auxList = pratosprincipais;
    } else if (pedido[ped].type == "sm") {
        auxList = sobremesas;
    }
    var i = 0;
    for (i = 0; i < auxList.length; i++) {
        if (pedido[ped].name == auxList[i].name) {
            break;
        }
    }
    auxList[i].numratings++;
    auxList[i].rating = (auxList[i].rating * (auxList[i].numratings - 1) + val) / auxList[i].numratings;
    var id = "p" + ped + "c";
    hide(id);
}

//Pagamento
var input = 0;
var chosen = 0;

function payment() {
    var modal = document.getElementById('paymentModal');
    var span = document.getElementsByClassName("close")[2];
    var dinheiro = document.getElementById("pagarDinheiro");
    var cartao = document.getElementById("pagarCartao");
    dinheiro.onclick = function() {
        modal.style.display = "none"
        paymentDinheiro();
    }
    cartao.onclick = function() {
        modal.style.display = "none"
        paymentCartao();
    }
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function paymentDinheiro() {
    var modal = document.getElementById('paymentDinheiroModal');
    var span = document.getElementsByClassName("close")[3];
    var p = document.getElementById("C2");
    p.innerHTML = "O custo total é: " + custoTotal.toFixed(2) + " €";
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function paymentCartao() {
    var modal = document.getElementById('paymentCartaoModal');
    var span = document.getElementsByClassName("close")[4];
    var p = document.getElementById("C1");
    p.innerHTML = "Escolha o tipo de cartão. O custo total é: " + custoTotal.toFixed(2) + " €";
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function changeCard(id) {
    if (id == "MasterC") {
        document.getElementById(id).className = "cartao-escolhido";
        document.getElementById("Visa").className = "cartao-img";
    }
    if (id == "Visa") {
        document.getElementById(id).className = "cartao-escolhido";
        document.getElementById("MasterC").className = "cartao-img";
    }
    chosen = 1;
}


function addToInput() {
    if (input < 4) {
        input++;
    }
    showInput();
}

function removeFromInput() {
    if (input > 0) {
        input--;
    }
    showInput();
}

function confirmInput() {
    if (input == 4 && chosen != 0) {
        var modal = document.getElementById('paymentCartaoModal');
        modal.style.display = "none";
        paymentConfirmed();
    }
}

function paymentConfirmed() {
    var modal = document.getElementById('paymentConfirmedModal');
    var span = document.getElementsByClassName("close")[5];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function showInput() {
    var p = document.getElementById("inputText");
    var s = "";
    for (var i = 1; i <= input; i++) {
        s += "⬤";
    }
    p.innerHTML = s;
}

function cancelInput() {
    var modal = document.getElementById('paymentCartaoModal');
    modal.style.display = "none";
}

function clearInput() {
    input = 0;
    showInput();
}

/* Modo Grupo */

var mesas = [];
var mesaIndex = -1;
var pedido0 = [];
var pedido1 = [];
var pedido2 = [];
var pedido3 = [];
var pedido4 = [];

mesas.push(pedido0);
mesas.push(pedido1);
mesas.push(pedido2);
mesas.push(pedido3);
mesas.push(pedido4);

function changeMesa(num) {
    pedido = mesas[num];
    mesaIndex = num;
    document.getElementById("mesa0").style.borderColor = "Black";
    document.getElementById("mesa1").style.borderColor = "Black";
    document.getElementById("mesa2").style.borderColor = "Black";
    document.getElementById("mesa3").style.borderColor = "Black";
    document.getElementById("mesa4").style.borderColor = "Black";
    var id = "mesa" + num;
    document.getElementById(id).style.borderColor = "Blue";
    forceStep('Entrada');
    showPedido();
}

function showAllPedido() {
    var total = 0;
    var ped = document.getElementById('Pedido');
    while (ped.hasChildNodes()) {
        ped.removeChild(ped.firstChild);
    }
    for (var p = 0; p < 5; p++) {
        for (var i = 0; i < mesas[p].length; i++) {
            total += mesas[p][i].price * mesas[p][i].units;
            var table = document.createElement('table');
            var item = document.createElement('li');
            item.style.maxWidth = "360px";
            item.style.marginLeft = "10pt";
            item.style.fontSize = "12pt";
            var unitcontainer = document.createElement('span');
            unitcontainer.appendChild(document.createTextNode(mesas[p][i].units + "x " + "(" + (p + 1) + ") "));
            unitcontainer.style.fontWeight = "bold";
            item.appendChild(unitcontainer);
            var itemcontainer = document.createElement('span');
            itemcontainer.appendChild(document.createTextNode(mesas[p][i].name + " = " + (mesas[p][i].price * mesas[p][i].units).toFixed(2) + "€"));
            if (p > 0 && mesas[p][i].type == "beal") {
                itemcontainer.style.color = "red";
            }
            item.appendChild(itemcontainer);
            var minus = document.createElement("img");
            minus.style.marginLeft = "10px";
            minus.src = "images/minus.png";
            minus.style.float = "right";
            minus.onclick = (function(p, i) {
                return function() {
                    removeFromPedido(mesas[nums[p]], mesas[nums[p]][nums[i]]);
                };
            })(nums[p], nums[i]);
            minus.className = "minusSign";
            var plus = document.createElement("img");
            plus.style.marginLeft = "10px";
            plus.src = "images/smallplus.png"
            plus.style.float = "right";
            plus.onclick = (function(p, i) {
                return function() {
                    addToPedido(mesas[nums[p]], mesas[nums[p]][nums[i]]);
                };
            })(nums[p], nums[i]);
            plus.className = "minusSign";
            item.appendChild(minus);
            item.appendChild(plus);
            if (mesas[p][i].type == "custom") {
                for (var j = 0; j < mesas[p][i].options.length; j++) {
                    var ili = document.createElement('li');
                    ili.appendChild(document.createTextNode("---- " + mesas[p][i].options[j].name));
                    if ((mesas[p][i].options[j].name == "Rum" ||
                        mesas[p][i].options[j].name == "Whiskey" ||
                        mesas[p][i].options[j].name == "Gin" ||
                        mesas[p][i].options[j].name == "Vodka" ||
                        mesas[p][i].options[j].name == "Tequila" )&& p > 0) {
                        ili.style.color = "red";
                    }
                    item.appendChild(ili);
                }
            }
            table.appendChild(item);
            ped.appendChild(table);
        }
    }
    if (total != 0) {
        var totaltext = document.createElement('p');
        totaltext.appendChild(document.createTextNode("Preço total: " + total.toFixed(2) + "€"));
        totaltext.style.fontSize = "14pt";
        ped.appendChild(totaltext);
    }
    custoTotal = total;
    pedido = mesas[0];
}
