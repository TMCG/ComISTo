var steps = ["Entrada", "Bebida", "Prato Principal", "Sobremesa", "Confirmar"];

function showIndividual() {
    hide("menuInicial");
    hide("AnotherBigDivBoys");
    show("ementaIndividual");
    show("individual");
    hide("mesa0");
    forceStep("Entrada");
}

function showGroup() {
    hide("menuInicial");
    hide("AnotherBigDivBoys");
    show("ementaIndividual");
    show("grupo");
    forceStep("Entrada");
    changeMesa(0);
}

function hide(id) {
    var element = document.getElementById(id);
    element.style.display = "none";
}

function show(id) {
    var element = document.getElementById(id);
    element.style.display = "inline";
}

function forceStep(id) {
    switchArrow(id);
    if (document.getElementById("Entrada").className == "current") {
        document.getElementById("Entrada").className = "passed";
    }
    if (document.getElementById("Bebida").className == "current") {
        document.getElementById("Bebida").className = "passed";
    }
    if (document.getElementById("Prato Principal").className == "current") {
        document.getElementById("Prato Principal").className = "passed";
    }
    if (document.getElementById("Sobremesa").className == "current") {
        document.getElementById("Sobremesa").className = "passed";
    }
    if (document.getElementById("Confirmar").className == "current") {
        document.getElementById("Confirmar").className = "";
    }
    document.getElementById("Ementa").style.display = "block";
    hide("ConfirmScreen");
    hide("ConfirmButton");
    hide("NotRecomendacoes");
    show("Pedido");
    show("Recomendacoes");
    if (id == "Confirmar") {
        document.getElementById("Entrada").className = "complete";
        document.getElementById("Bebida").className = "complete";
        document.getElementById("Prato Principal").className = "complete";
        document.getElementById("Sobremesa").className = "complete";
        hide("Ementa");
        hide("Pedido");
        hide("Recomendacoes");
        show("NotRecomendacoes");
        show("ConfirmScreen");
        show("ConfirmButton");
        showConfirm();
        document.getElementById(id).className = "current";
        return;
    }
    document.getElementById(id).className = "current";
    var ementa = document.getElementById("Ementa");
    while (ementa.hasChildNodes()) {
        ementa.removeChild(ementa.firstChild);
    }
    ementa.appendChild(showEmenta(id));
    var recomendacoes = document.getElementById("Recomendacoes");
    while (recomendacoes.hasChildNodes()) {
        recomendacoes.removeChild(recomendacoes.firstChild);
    }
    recomendacoes.appendChild(showRecomendacoes(id));
    ementa.scrollTop = 0;
    recomendacoes.scrollLeft = 0;

}

function nextStep(id) {
    if (id == "Entrada") {
        show("LeftArrow");
        forceStep("Bebida");
        document.getElementById("Entrada").className = "complete";
    }
    if (id == "Bebida") {
        forceStep("Prato Principal");
        document.getElementById("Bebida").className = "complete";
    }

    if (id == "Prato Principal") {
        forceStep("Sobremesa");
        document.getElementById("Prato Principal").className = "complete";
    }
    if (id == "Sobremesa") {
        forceStep("Confirmar");
        document.getElementById("Sobremesa").className = "complete";
    }
    if (id == "Confirmar") {
        document.getElementById("Confirmar").className = "";
    }
}

function switchArrow(id) {
    show("RightArrow");
    show("LeftArrow");
    hide("leftpad");
    if (id == "Entrada") {
        hide("LeftArrow");
        show("leftpad");
        document.getElementById("RightA").onclick = (function() {
            return function() {
                nextStep("Entrada");
            };
        })();

        document.getElementById("LeftA").onclick = (function() {
            return function() {
                previousStep("Entrada");
            };
        })();
    }
    if (id == "Bebida") {
        document.getElementById("RightA").onclick = (function() {
            return function() {
                nextStep("Bebida");
            };
        })();

        document.getElementById("LeftA").onclick = (function() {
            return function() {
                previousStep("Bebida");
            };
        })();
    }

    if (id == "Prato Principal") {
        document.getElementById("RightA").onclick = (function() {
            return function() {
                nextStep("Prato Principal");
            };
        })();

        document.getElementById("LeftA").onclick = (function() {
            return function() {
                previousStep("Prato Principal");
            };
        })();
    }
    if (id == "Sobremesa") {
        document.getElementById("RightA").onclick = (function() {
            return function() {
                nextStep("Sobremesa");
            };
        })();

        document.getElementById("LeftA").onclick = (function() {
            return function() {
                previousStep("Sobremesa");
            };
        })();
    }
    if (id == "Confirmar") {
        hide("RightArrow");
        document.getElementById("RightA").onclick = (function() {
            return function() {
                nextStep("Confirmar");
            };
        })();

        document.getElementById("LeftA").onclick = (function() {
            return function() {
                previousStep("Confirmar");
            };
        })();

    }
}

function previousStep(id) {
    if (id == "Entrada") {
        hide("LeftArrow");
        document.getElementById("Entrada").className = "complete";
    }
    if (id == "Bebida") {
        forceStep("Entrada");
        document.getElementById("Bebida").className = "complete";
    }

    if (id == "Prato Principal") {
        forceStep("Bebida");
        document.getElementById("Prato Principal").className = "complete";
    }
    if (id == "Sobremesa") {
        forceStep("Prato Principal");
        document.getElementById("Sobremesa").className = "complete";
    }
    if (id == "Confirmar") {
        show("RightArrow");
        forceStep("Sobremesa");
        document.getElementById("Confirmar").className = "";
    }
}

function help() {
    var modal = document.getElementById('helpModal');
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}


function overCustom() {
    var modal = document.getElementById('overCustomModal');
    var span = document.getElementsByClassName("close")[3];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function repeatCustomModal() {
    var modal = document.getElementById('repeatCustomModal');
    var span = document.getElementsByClassName("close")[5];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}


function noIngredModal() {
    var modal = document.getElementById('noIngredModal');
    var span = document.getElementsByClassName("close")[3];
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    modal.style.display = "block";
}

function goToWaiting() {
    hide("BigDivBoys");
    show("AnotherBigDivBoys");
    showRequested();
    if (mesaIndex > 0) {
        hide("WaitingPagamento");
        show("WaitingThingy");
    } else {
      show("WaitingPagamento");
      hide("WaitingThingy");
    }
}

function alterarPedido() {
    show("BigDivBoys");
    hide("AnotherBigDivBoys");
    forceStep("Entrada");
}

function cancelPrompt() {
    var modal = document.getElementById('cancelModal');
    var span = document.getElementsByClassName("close")[1];
    span.onclick = function() {
        modal.style.display = "none";
    }
    var yesBtn = document.getElementById("yesBtnCancel");
    var noBtn = document.getElementById("noBtnCancel");
    yesBtn.onclick = function() {
        location.reload();
    }
    noBtn.onclick = function() {
        modal.style.display = "none";
    }
    modal.style.display = "block";
}
